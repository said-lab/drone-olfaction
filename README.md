This repository contains a database extracted during an experiment conduced in Wageningen University & Research. It is composed of multiple geographically labeled points with information about the nitrogen dioxide (NO2) content in the atmosphere as measured by an eletrical sensor attached to a UAV. A brief introduction can be seen in the following [video](https://youtu.be/l7Z-QbNZGIc).

![](MDPI_micromachines.m4v)

# Usage

Two sets build the whole database: one collected by the UAV and other by the sensor. Because the UAV's GPS is more precise, but the NO2 information was collected by the sensor, both databases had to be combined.

In this repository, data is organized in two folders according to the date of collection, 15 and 20th of June 2018, ***15-06*** and ***20-06*** respectively. Each of these contains sub-folders for the raw data of the ***sensor*** and the ***UAV***. Files are named according to their pattern plus a capital letter to indicate which datasets from sensor and UAV are matches. The UAV files names also indicate the initial time when data was collected (exaclty 2 hours ahead of sensor time). Additionaly, ***drone_data.csv*** contains the merged data.

For merging, latitude, longitude, altitude and time elapsed between measurements were extracted from UAV. From sensors, only the gas measurement, under the column WE.AE, was used.

More information on merging and data analysis can be found in the [article](https://www.mdpi.com/2072-666X/11/8/768/htm) written based on this database.
![](https://i.imgur.com/P3IcAgX.png)



# License

Any work done using this databse should reference the paper [Experimental Flight Patterns Evaluation for a UAV-Based Air Pollutant Sensor](https://www.mdpi.com/2072-666X/11/8/768/htm) by [João O. Araujo](https://sciprofiles.com/profile/1178625), [João Valente](https://sciprofiles.com/profile/6111), [Lammert Kooistra](https://sciprofiles.com/profile/10977), [Sandra Munniks](https://sciprofiles.com/profile/author/RyswekxMSmJ5SVZ2Tm1vYW1JUDFjdHY5RTRyM1ltQVJhanJnSEsvTHdjcz0=) and [R.J.B Peters](https://sciprofiles.com/profile/465797).
```
@article{Araujo2020,
abstract = {{\textless}p{\textgreater}The use of drones in combination with remote sensors have displayed increasing interest over the last years due to its potential to automate monitoring processes. In this study, a novel approach of a small flying e-nose is proposed by assembling a set of AlphaSense electrochemical-sensors to a DJI Matrix 100 unmanned aerial vehicle (UAV). The system was tested on an outdoor field with a source of NO2. Field tests were conducted in a 100 m2 area on two dates with different wind speed levels varying from low (0.0–2.9m/s) to high (2.1–5.3m/s), two flight patterns zigzag and spiral and at three altitudes (3, 6 and 9 m). The objective of this study is to evaluate the sensors responsiveness and performance when subject to distinct flying conditions. A Wilcoxon rank-sum test showed significant difference between flight patterns only under High Wind conditions, with Spiral flights being slightly superior than Zigzag. With the aim of contributing to other studies in the same field, the data used in this analysis will be shared with the scientific community.{\textless}/p{\textgreater}},
author = {Araujo, Jo{\~{a}}o Ot{\'{a}}vio and Valente, Jo{\~{a}}o and Kooistra, Lammert and Munniks, Sandra and Peters, Ruud J. B.},
doi = {10.3390/mi11080768},
file = {:C$\backslash$:/Users/joaor/AppData/Local/Mendeley Ltd./Mendeley Desktop/Downloaded/Araujo et al. - 2020 - Experimental Flight Patterns Evaluation for a UAV-Based Air Pollutant Sensor.pdf:pdf},
issn = {2072-666X},
journal = {Micromachines},
keywords = {electrochemical sensors,gas sensing,remote sensing,unmanned aerial vehicle},
month = {aug},
number = {8},
pages = {768},
publisher = {Multidisciplinary Digital Publishing Institute},
title = {{Experimental Flight Patterns Evaluation for a UAV-Based Air Pollutant Sensor}},
url = {https://www.mdpi.com/2072-666X/11/8/768},
volume = {11},
year = {2020}
}
```
